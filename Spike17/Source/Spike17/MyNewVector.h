// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MyNewVector.generated.h"

UCLASS()
class SPIKE17_API AMyNewVector : public AActor
{
	GENERATED_BODY()

private:
	float _x;
	float _y;
	float _z;
public:
	// Sets default values for this actor's properties
	AMyNewVector();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	UFUNCTION(BlueprintCallable, Category = "Math")
		float getX();
	UFUNCTION(BlueprintCallable, Category = "Math")
		float getY();
	UFUNCTION(BlueprintCallable, Category = "Math")
		float getZ();

	UFUNCTION(BlueprintCallable, Category = "Math")
		void setX(float value);
	UFUNCTION(BlueprintCallable, Category = "Math")
		void setY(float value);
	UFUNCTION(BlueprintCallable, Category = "Math")
		void setZ(float value);
	UFUNCTION(BlueprintCallable, Category = "Math")
		void addVector(AMyNewVector* vector);
	UFUNCTION(BlueprintCallable, Category = "Math")
		void subtractVector(AMyNewVector* vector);

	UFUNCTION(BlueprintCallable, Category = "Math")
		void multiply(float value);

	UFUNCTION(BlueprintCallable, Category = "Math")
		void division(float value);

	UFUNCTION(BlueprintCallable, Category = "Math")
		float dotProduct(AMyNewVector* vector);

	UFUNCTION(BlueprintCallable, Category = "Math")
		void setValues(float x, float y, float z);

};
