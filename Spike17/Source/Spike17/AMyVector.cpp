// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike17.h"
#include "AMyVector.h"

AMyVector::AMyVector(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}


float AMyVector::getX()
{
	return _x;
}

float AMyVector::getY()
{
	return _y;
}

float AMyVector::getZ()
{
	return _z;
}

void AMyVector::setX(float value)
{
	_x = value;
}

void AMyVector::setY(float value)
{
	_y = value;
}

void AMyVector::setZ(float value)
{
	_z = value;
}