// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include	"GameFramework/Actor.h"
#include "AMyVector.generated.h"

/**
 *
 */
UCLASS()
class SPIKE17_API AMyVector : public AActor
{
	GENERATED_BODY()

private:
	float _x;
	float _y;
	float _z;
public:
	AMyVector(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Math")
		float getX();
	UFUNCTION(BlueprintCallable, Category = "Math")
		float getY();
	UFUNCTION(BlueprintCallable, Category = "Math")
		float getZ();

	UFUNCTION(BlueprintCallable, Category = "Math")
		void setX(float value);
	UFUNCTION(BlueprintCallable, Category = "Math")
		void setY(float value);
	UFUNCTION(BlueprintCallable, Category = "Math")
		void setZ(float value);
};
