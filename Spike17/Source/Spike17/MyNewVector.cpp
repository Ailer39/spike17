// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike17.h"
#include "MyNewVector.h"
#include <iostream>

using namespace std;


// Sets default values
AMyNewVector::AMyNewVector()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyNewVector::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyNewVector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AMyNewVector::getX()
{
	return _x;
}

float AMyNewVector::getY()
{
	return _y;
}

float AMyNewVector::getZ()
{
	return _z;
}

void AMyNewVector::setX(float value)
{
	_x = value;
}

void AMyNewVector::setY(float value)
{
	_y = value;
}

void AMyNewVector::setZ(float value)
{
	_z = value;
}

void AMyNewVector::addVector(AMyNewVector* vector)
{
	_x += vector->getX();
	_y += vector->getY();
	_z += vector->getZ();
}

void AMyNewVector::subtractVector(AMyNewVector* vector)
{
	_x -= vector->getX();
	_y -= vector->getY();
	_z -= vector->getZ();
}

void AMyNewVector::multiply(float value)
{
	_x *= value;
	_y *= value;
	_z *= value;
}

void AMyNewVector::division(float value)
{
	_x /= value;
	_y /= value;
	_z /= value;
}

float AMyNewVector::dotProduct(AMyNewVector* vector)
{
	float x = _x * vector->getX();
	float y = _y * vector->getY();
	float z = _z * vector->getZ();
	cout << "X: " << x;
	cout << "Y: " << y;
	cout << "Z: " << z;
	return y + z;
}

void AMyNewVector::setValues(float x, float y, float z)
{
	_x = x;
	_y = y;
	_z = z;
}